.PHONY: all build push clean

BUILDCMD ?= docker
MYREPO ?= goozbachinfra/tt-rss

all: build

build:
	$(BUILDCMD) build -t $(MYREPO) .

push: build
	$(BUILDCMD) push $(MYREPO)

clean:
	$(BUILDCMD) rmi -f $(MYREPO)
