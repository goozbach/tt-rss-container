FROM php:7.3-apache

RUN apt-get update && apt-get install -y libpq-dev libicu-dev && docker-php-ext-install pdo pdo_pgsql intl pgsql

COPY upstream /var/www/html

COPY config.php /var/www/html

RUN chown -R www-data /var/www/html

